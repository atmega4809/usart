//! \file
//! \addtogroup USART
//! \brief Interrupt driven USART Lib for ATmega4809
//! \details 
//! \author Luke Paulson
//! \date May 21, 2020
//! \copyright The MIT License (MIT) 
//! \version 



#ifndef USART_H_
#define USART_H_

#include <stdint.h>
#include <string.h>
#include <avr/io.h>
#include <stdbool.h>
#include <avr/interrupt.h>

#include "rgbuffer.h"


#define USART_BAUD_RATE(BAUD_RATE) ((float)(F_CPU * 64 / (16 * (float)BAUD_RATE)) + 0.5)

typedef uint16_t USART_BAUD_t;
typedef bool USART_DRE_t;
typedef bool USART_TXC_t;
typedef bool USART_RXC_t;

typedef enum USART_ACCESS_ENUM{
  USART_READ = 0,
  USART_WRITE = 1,
  USART_READWRITE = 2,
  USART_MODE_MAX
}USART_ACCESS_t;

typedef struct USART_CHANNEL_STRUCTURE{ 
  USART_t* usart_ch;
  RingbufChar_s rb;
  USART_ACCESS_t mode;
  USART_DRE_t dataRegEmpty;
  USART_TXC_t txComplete;
  USART_RXC_t rxComplete;
}USART_CH_t;

void usart_init(USART_CH_t* usart, 
                USART_t* ch, 
                USART_ACCESS_t mode,
                USART_BAUD_t baud);     //!< Initilize user usart

void usart_start(USART_CH_t* usart);                          //!< Start the user usart and enable settings
void usart_set_baud(USART_CH_t* usart, USART_BAUD_t baud);        //!< set the baud rate of user usart

void usart_enable_tx(USART_CH_t* usart);          //!< Enable usart tx channel
void usart_disable_tx(USART_CH_t* usart);       //!< Disabl usart tx channel
void usart_enable_txc_int(USART_CH_t* usart);   //!< Enable tx interrupts
void usart_disable_txc_int(USART_CH_t* usart);  //!< Disable tc interrupts
void usart_enable_dre_int(USART_CH_t* usart);   //!< Enable DRE interrupt
void usart_disable_dre_int(USART_CH_t* usart);  //!< Disable DRE interrupt

void usart_enable_rx(USART_CH_t* usart);        //!< Enable usart rx channel  
void usart_diable_rx(USART_CH_t* usart);        //!< Disable usart rx chanel
void usart_enable_rxc_int(USART_CH_t* usart);   //!< Enable RX complete interrupts
void usart_disable_rxc_int(USART_CH_t* usart);  //!< Disable  interrupts
void usart_enable_rxs_int(USART_CH_t* usart);   //!< Enable RX start frame
void usart_disable_rxs_int(USART_CH_t* usart);  //!< Disable RX strt frame

bool usart_send_char(USART_CH_t* usart, char c);         //!< Send single byte
uint8_t usart_send_n_char(USART_CH_t* usart, char* s);  //!< Send n bytes

char usart_read_char(USART_CH_t* usart);        //!< Read single byte from usart
uint8_t usart_read_n_char(  USART_CH_t* usart, 
                            char* dest, 
                            size_t num);        //!< Read n bytes from usart





#endif /* USART_H_ */