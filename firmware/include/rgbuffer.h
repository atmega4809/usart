//! \file
//! \addtogroup RINGBUFFER
//! \brief Simple Ringbuffer library
//! \details 
//! \author Luke Paulson
//! \date June 1, 2020
//! \copyright The MIT License (MIT) 
//! \version 


#ifndef RGBUFFER_H
#define RGBUFFER_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define write(b,v)      b->write(b,v)
#define write_n(b,p,s)  b->write_n(b,p,s)
#define read(b)         b->read(b)
#define read_n(b,p,s)   b->read_n(b,p,s)
#define delete_buf(b)   b->delete(b)
#define flush_buf(b)    b->flush(b)
#define buf_empty(b)    b->is_empty(b)
#define buf_full(b)     b->is_full(b)

typedef struct RingbufChar_struct RingbufChar_s;

typedef void (*fptrDelete_char)(RingbufChar_s*);
typedef bool (*fptrWrite_char)(RingbufChar_s*, char);
typedef char (*fptrRead_char)(RingbufChar_s*);
typedef uint8_t (*fptrWrite_n_char)(RingbufChar_s*, char*, size_t);
typedef uint8_t (*fptrRead_n_char)(RingbufChar_s*, char*, size_t);
typedef void (*fptrFlush_char)(RingbufChar_s*);
typedef bool (*fptrEmpty_char)(RingbufChar_s*);
typedef bool (*fptrFull_char)(RingbufChar_s*);

#define MAX_BUF_LEN 32         //!< Maximum buffer size

//! Ringbuffer object
struct RingbufChar_struct{
  char data[MAX_BUF_LEN];                       //!< Pointer to data
  size_t head;                      //!< Pointer to head of buffer
  size_t tail;                      //!< Pointer to tail of buffer
  size_t size;                      //!< Size of buffer in bytes
  bool empty;                       //!< Empty Flag - Set when tail = head
  bool full;                        //!< Full flag - Set when head = tail

  fptrDelete_char delete;
  fptrWrite_char write;
  fptrWrite_n_char write_n;
  fptrRead_char read;
  fptrRead_n_char read_n;
  fptrFlush_char flush;
  fptrEmpty_char is_empty;
  fptrFull_char is_full;
};

void init_ringBuffer(RingbufChar_s* self);                            //!< Create Ringbuffer
void delete_buf_char(RingbufChar_s*);                             //!< Destroy Ringbuffer
bool write_char(RingbufChar_s*, char);                         //!< Write single byte
char read_char(RingbufChar_s*);                                  //!< Read single char
uint8_t write_char_n(RingbufChar_s*, char*, size_t);          //!< Write 'n' number of bytes
uint8_t read_char_n(RingbufChar_s*, char*, size_t);        //!< Read 'n' number of bytes
void flush_buf_char(RingbufChar_s*);                                  //!< Flush the buffer
bool is_full_char(RingbufChar_s*);
bool is_empty_char(RingbufChar_s*);

#endif // RGBUFFER_H
