//! \file
//! \addtogroup TEST
//! \brief Ceedling test file for rgbuffer.c
//! \details 
//! \author Luke Paulson
//! \date June 1, 2020
//! \copyright The MIT License (MIT)
//! \version  

#include "unity.h"

#include <string.h>

#include "rgbuffer.h"

static RingbufChar_s rgBuffer;
RingbufChar_s* test_buffer_1 = &rgBuffer;                     //!< Device under test

char temp_buffer[MAX_BUF_LEN];
char* temp_buffer_p = temp_buffer;

//! Setup function called before each test
void setUp(void){                                   
 init_ringBuffer(test_buffer_1); 
}
//! Teardown function called after each test 
void tearDown(void){
  flush_buf(test_buffer_1);
}

//! \test Write a single byte to the an empty Ring Buffer
void test_write_single_byte_to_empty_buffer(void){
    TEST_ASSERT_TRUE(write(test_buffer_1, 'c'));
}

//! \test Write a single byte to a full buffer 
void test_write_single_byte_to_full_buffer(void){
    test_buffer_1->full = true;
    TEST_ASSERT_FALSE(write(test_buffer_1, 'c'));
}

//! \test Read from empty buffer
void test_read_from_empty_buffer(void){
  TEST_ASSERT_EQUAL_CHAR('\0', read(test_buffer_1));
}
//! \test Write then read a single byte
void test_write_the_read(void){
  write(test_buffer_1, 'c');
  TEST_ASSERT_EQUAL_CHAR('c', read(test_buffer_1));
}
//! \test Write multiple bytes
void test_write_multiple_bytes(void){
  TEST_ASSERT_EQUAL_UINT8(11, write_n(test_buffer_1, "hello world", 11));
}
//! \test Write multiple byte then read multiple bytes
void test_write_multiple_read_multiple_num(void){
  write_n(test_buffer_1, "hello world", 11);
  TEST_ASSERT_EQUAL_UINT8(11, read_n(test_buffer_1, temp_buffer_p, 11));
}

//! \test Write multiple byte then read multiple bytes
void test_write_multiple_read_multiple(void){
  write_n(test_buffer_1, "hello world", 11);
  read_n(test_buffer_1, temp_buffer_p, 11);
  TEST_ASSERT_EQUAL_STRING("hello world", temp_buffer_p);
}
//! \test Write more than maximum buffer size
void test_write_more_than_max_buffer(void){
  char* test_str = "This is a this is a massive string that should be larger than than max buff";
  size_t string_length = strlen(test_str);
  if(string_length < MAX_BUF_LEN){
    TEST_FAIL_MESSAGE("Test string is smaller than buffer size");
  }
  else{
    TEST_ASSERT_EQUAL_UINT8(MAX_BUF_LEN, write_n(test_buffer_1, test_str, string_length));
  }
}
  
//! \test Cyclic write read
void test_cycle_read_write_sequence(void){
  TEST_ASSERT_EQUAL_UINT(10, write_n(test_buffer_1, "abcdefghij", 10));
  TEST_ASSERT_EQUAL_UINT8(5, read_n(test_buffer_1, NULL, 5));
  TEST_ASSERT_EQUAL_UINT(10, write_n(test_buffer_1, "abcdefghij", 10));
  TEST_ASSERT_EQUAL_UINT8(5, read_n(test_buffer_1, NULL, 5));
  TEST_ASSERT_EQUAL_UINT(6, write_n(test_buffer_1, "abcdefghij", 10));
}

//! \test Flush buffer
void test_flush_buffer(void){
  write_n(test_buffer_1, "bigbigbigbigbigbig", 18);
  TEST_ASSERT_FALSE(write(test_buffer_1, 'c'));
  flush_buf(test_buffer_1);
  TEST_ASSERT_TRUE(write(test_buffer_1, 'c'));
}

//! \test Read entire buffer
void test_read_entire_buffer(void){
  write_n(test_buffer_1, "Hello world", 11);
  TEST_ASSERT_EQUAL_UINT8(11, read_n(test_buffer_1, temp_buffer_p, -1));
}

//! \test Test full flag
void test_full_flag(void){
  TEST_ASSERT_FALSE(buf_full(test_buffer_1));
  write_n(test_buffer_1, "bigbigbigbigbigbig", 18);
  TEST_ASSERT_TRUE(buf_full(test_buffer_1));
}

//! \test Test empty flag
void test_empty_flag(void){
  TEST_ASSERT_TRUE(buf_empty(test_buffer_1));
  write(test_buffer_1, 'c');
  TEST_ASSERT_FALSE(buf_empty(test_buffer_1));
}