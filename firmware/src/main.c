/*
 * usart.c
 *
 * Created: 5/21/2020 2:38:02 PM
 * Author : Luke
 */ 
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "usart.h"

int main(void)
{
    /* Replace with your application code */
		PORTF.DIRSET = PIN5_bm;
		
    USART_CH_t usart3;
    
    usart_init(&usart3, &USART3, USART_WRITE, 9600);
    usart_start(&usart3);
    
    while (1) 
    {
			usart_send_n_char(&usart3, "Hello World!\n");
			_delay_ms(500);
			PORTF.OUTTGL = PIN5_bm;

    }
}

