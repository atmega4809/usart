//! \brief Interrupt driven USART Lib for ATmega4809
//! \details 
//! \author Luke Paulson
//! \date May 21, 2020
//! \copyright The MIT License (MIT) 
//! \version


#include "usart.h"

/*!	Attach hardware usart structure to user structure and configure the usart access more
	\param usart User usart defined in the module it'll be used in
	\param ch	Hardware usart sturture
	\param mode Usart access mode to tertermine which direction to enabled
	\param baud Usart baud rate
*/


void usart_init(USART_CH_t* usart, 
								USART_t* ch, 
								USART_ACCESS_t mode, 
								USART_BAUD_t baud)
{
	if( mode > USART_MODE_MAX ){
		mode = USART_READWRITE;
	}
	usart->usart_ch = ch;
	usart->mode = mode;
  usart->dataRegEmpty  = true;
  usart->txComplete = false;
  usart->rxComplete = false;
  
  usart_set_baud(usart, baud);							//Set baud rate
  
	return;
}
/*!	Start the Usart by enabling the approriate setting based on the mode 

*/
void usart_start(USART_CH_t* usart){
	
	switch(usart->mode){
		case(USART_READ):
			usart_enable_rx(usart);
			break;
		case(USART_WRITE):
			usart_enable_tx(usart);
			break;
		case(USART_READWRITE):
			usart_enable_rx(usart);
			usart_enable_tx(usart);
			break;
		default:
			usart_enable_rx(usart);
			usart_enable_tx(usart);
			break;
		}

	return;
}


void usart_set_baud(USART_CH_t* usart, USART_BAUD_t baud){
	usart->usart_ch->BAUD = (uint16_t)USART_BAUD_RATE(baud);
	return;
}

void usart_enable_tx(USART_CH_t* usart){
	usart->usart_ch->CTRLB |= USART_TXEN_bm;
  if(usart->usart_ch == (USART_t*)0x800){
    PORTA.DIRSET = PIN0_bm;     //USART0 TX Output
  }
  else if(usart->usart_ch == (USART_t*)0x820){
    PORTC.DIRSET = PIN0_bm;     //USART1 TX Output
  }
  else if(usart->usart_ch == (USART_t*)0x840){
    PORTF.DIRSET = PIN0_bm;     //USART2 TX Output
  }
  else if(usart->usart_ch == (USART_t*)0x860){
    PORTB.DIRSET = PIN0_bm;     //USART3 TX Output
  }
  return;
}

void usart_disable_tx(USART_CH_t* usart){
	usart->usart_ch->CTRLB &= ~USART_TXEN_bm;
	return;
}

void usart_enable_txc_int(USART_CH_t* usart){
	usart->usart_ch->CTRLA |= USART_TXCIE_bm;
	return;
}

void usart_disable_txc_int(USART_CH_t* usart){
	usart->usart_ch->CTRLA &= ~USART_TXCIE_bm;
	return;
}

void usart_enable_dre_int(USART_CH_t* usart){
  usart->usart_ch->CTRLA |= USART_DREIE_bm;
  return;
}

void usart_disable_dre_int(USART_CH_t* usart){
  usart->usart_ch->CTRLA &= ~USART_DREIE_bm;
  return;
}

void usart_enable_rx(USART_CH_t* usart){
  usart->usart_ch->CTRLB |= USART_RXEN_bm;
  if(usart->usart_ch == (USART_t*)0x800){
    PORTA.DIRCLR = PIN1_bm;     //USART0 RX Output
  }
  else if(usart->usart_ch == (USART_t*)0x820){
    PORTC.DIRCLR = PIN1_bm;     //USART1 RX Output
  }
  else if(usart->usart_ch == (USART_t*)0x840){
    PORTF.DIRCLR = PIN1_bm;     //USART2 RX Output
  }
  else if(usart->usart_ch == (USART_t*)0x860){
    PORTB.DIRCLR = PIN1_bm;     //USART3 RX Output
  }
  return;
	return;
}

void usart_diable_rx(USART_CH_t* usart){
	usart->usart_ch->CTRLB &= ~USART_RXEN_bm;
	return;	
}

void usart_enable_rxc_int(USART_CH_t* usart){
	usart->usart_ch->CTRLA |= USART_RXCIE_bm;
	return;
}

void usart_disable_rxc_int(USART_CH_t* usart){
	usart->usart_ch->CTRLA &= ~USART_RXCIE_bm;	
	return;
}

void usart_enable_rxs_int(USART_CH_t* usart){
  usart->usart_ch->CTRLA |= USART_RXSIE_bm;
  return;
}

void usart_diable_rxs_int(USART_CH_t* usart){
  usart->usart_ch->CTRLA &= ~USART_RXSIE_bm;
  return;
}

bool usart_send_char(USART_CH_t* usart, char c){
	while (!(usart->usart_ch->STATUS & USART_DREIF_bm)){;}
	usart->usart_ch->TXDATAL = c;
	return true;
}

uint8_t usart_send_n_char(USART_CH_t* usart, char* s){
	for(size_t i = 0; i < strlen(s); i++){
		usart_sendChar(usart, s[i]);
	}
	return 0;
}

char usart_read_char(USART_CH_t* usart){
	return 0;
}

uint8_t usart_read_n_char(USART_CH_t* usart, char* dest, size_t num){
	return 0;
}


//Interrupt Service Routine
/*
 * USART Data Receive Complete 
 * ISR(USART0_RXC_vect){}
 * ISR(USART1_RXC_vect){}
 * ISR(USART2_RXC_vect){}
 * ISR(USART3_RXC_vect){}
 * 
 * USART Data Register Empty Flag
 * ISR(USART0_DRE_vect){}
 * ISR(USART1_DRE_vect){}
 * ISR(USART2_DRE_vect){}
 * ISR(USART3_DRE_vect){}
 * 
 * USART Data Transmission Complete
 * ISR(USART0_TXC_vect){}
 * ISR(USART1_TXC_vect){} 
 * ISR(USART2_TXC_vect){} 
 * ISR(USART3_TXC_vect){}  
 */

//Example

/*
ISR(USART3_RXC_vect){
	//Check for RX errors
	bool error = false;
	error |= (USART3.RXDATAH & USART_BUFOVF_bm);     //Check for buffer overflow
  error |= (USART3.RXDATAH & USART_FERR_bm);       //Check for frame error
  error |= (USART3.RXDATAH & USART_PERR_bm);       //Check for parity error
  if(error){        //If there is an error clear the buffer
    
    return;
  }
}

ISR(USART3_DRE_vect){
  //If usart3 ring buffer is not empty load tx data register with next value
  if(!buf_empty(usart3_txData)){
    USART3.TXDATAL = read(usart3_txData)      
  } 
  return;
}

ISR(USART3_TXC_vect){
  USART3.CTRLA &= ~USART_TXCIE_bm;       //Disable TX interrupt
  txComplete = True;
}
*/