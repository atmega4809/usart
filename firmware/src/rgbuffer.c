//! \file 
//! \addtogroup RingbufChar_sFER
//! \author Luke Paulson
//! \date June 1, 2020
//! \copyright The MIT License (MIT) 
//! \version 

#include "rgbuffer.h"

/*!
  \param Ringbuffer self object
*/
void init_ringBuffer(RingbufChar_s* self){

  //Initilize data
  self->head = 0;
  self->tail = 0;
  self->size = MAX_BUF_LEN;
  self->empty  = true;
  self->full = false;

  //Attach function pointers
  self->delete = &delete_buf_char;
  self->write = &write_char;
  self->write_n = &write_char_n;
  self->read = &read_char;
  self->read_n = &read_char_n;
  self->flush = &flush_buf_char;
  self->is_empty = &is_empty_char;
  self->is_full = &is_full_char;

  return;
}


void delete_buf_char(RingbufChar_s* self){
  //Do nothing
  return;
}

/*!
  \retval TRUE Character written
  \retval FALSE Character not written
*/
bool write_char(RingbufChar_s* self, char c){
  //check that bufer is not full
  if(self->full){
    return false;
  }
  //buffer no longer empty
  self->empty = false;
  
  self->data[self->head] =  c;
  //check head is not past data length
  if(++self->head > self->size-1){
    self->head = 0;
  }

  //check that head is not equal tail
  if(self->head == self->tail){
    self->full = true;
  }

  return true;
}

/*!
  \retval NULL Buffer Empty
*/
char read_char(RingbufChar_s* self){
  //check if buffer empty
  if(self->empty){
    return '\0';    
  }
  char buf;
  
  //buffer no longer full
  self->full = false;
  
  buf = self->data[self->tail];
  //check tail is not past data length
  if(++self->tail > self->size-1){
    self->tail = 0;
  }
  //check that tail not equal to head
  if(self->tail == self->head){
    self->empty = true;
  }
  
  return buf;
}

/*!
  \param self Ring Buffer Object
  \param s Pointer to array of bytes
  \param n Number of bytes to write

  \return Actual number of bytes writen
*/
uint8_t write_char_n(RingbufChar_s* self, char* s, size_t n){
  if(n > MAX_BUF_LEN){
    n = MAX_BUF_LEN;
  }
  uint8_t i;
  for(i = 0; i < n; i++){
    //write untill buffer full
    if(!(write(self, s[i]))){
      return i;
    }
  }
  return i;
}

/*!
  \param self Ring Buffer Object
  \param dest Pointer to destination buffer
  \param n Number of bytes to read

  \return Number of bytes actually read
*/
uint8_t read_char_n(RingbufChar_s* self, char* dest, size_t n){
  char* temp_buf_p;
  //Check for oversized buffer
  if(n > MAX_BUF_LEN){
    n = MAX_BUF_LEN; 
  }

  char temp_buf[n];
  temp_buf_p = temp_buf;
  
  for(size_t i = 0; i < n; i++){
    //check for null char
    temp_buf[i] = read(self);
    if (temp_buf[i] == '\0'){
      return i;
    }
  }
  
  //copy temp_buffer into destination
  if(dest != NULL){
    dest = memcpy(dest, temp_buf, n);
  }
  
  return n;
}

/*!

*/
void flush_buf_char(RingbufChar_s* self){
  self->empty = true;
  self->full = false;
  self->tail = self-> head;
  return;
}

//!
bool is_full_char(RingbufChar_s* self){
  return self->full;
}

bool is_empty_char(RingbufChar_s* self){
  return self->empty;
}