@mainpage

this is the mainpage


## Setting-up AVR-gcc toolchain and AVRdude in Ubuntu
----
### Requirments
1. binutils
2. gcc-avr
3. avr-libc
4. avrdude
5. srecord
   
### Optional [For Debugging]
1. gdb-avr
2. simulavr
   
### Setting Up Enviroment

### Compiler Script
```bash
    avr-gcc -g -Os -mmcu=“microcontroller” -c “filename”.c
    avr-gcc -g -mmcu=“microcontroller” -o “filename”.elf “filename”.o
    avr-objcopy -j .text -j .data -O ihex “filename”.elf “filename”.hex
    
    avr-size --format=avr --mcu=“microcontroller” “filename”.elf    
```

What does this do?
```bash
    avr-gcc -g -Os -mmcu=“microcontroller” -c “filename”.c
```
run avr-gcc optomized for speed, for the specified microcontroller, compile the file but do-not link (-g is for debugging)

```bash
avr-gcc -g -mmcu=“microcontroller” -o “filename”.elf “filename”.o
``` 
run avr-gcc again but this time output the files .elf and .o

```bash
avr-objcopy -j .text -j .data -O ihex “filename”.elf “filename”.hex
```




## AVR-GCC Flags
```
  --target-help            Display target specific command line options
  --help={common|optimizers|params|target|warnings|[^]{joined|separate|undocumented}}[,...]
                           Display specific types of command line options
  (Use '-v --help' to display command line options of sub-processes)
  -print-search-dirs       Display the directories in the compiler's search path
  -print-libgcc-file-name  Display the name of the compiler's companion library

  -Wa,<options>            Pass comma-separated <options> on to the assembler
  -Wp,<options>            Pass comma-separated <options> on to the preprocessor
  -Wl,<options>            Pass comma-separated <options> on to the linker
  
  -Xassembler <arg>        Pass <arg> on to the assembler
  -Xpreprocessor <arg>     Pass <arg> on to the preprocessor
  -Xlinker <arg>           Pass <arg> on to the linker
  
  -save-temps              Do not delete intermediate files
  -save-temps=<arg>        Do not delete intermediate files
  -no-canonical-prefixes   Do not canonicalize paths when building relative
                           prefixes to other gcc components
  -pipe                    Use pipes rather than intermediate files
  -time                    Time the execution of each subprocess

  -std=<standard>          Assume that the input sources are for <standard>
  --sysroot=<directory>    Use <directory> as the root directory for headers
                           and libraries
  -B <directory>           Add <directory> to the compiler's search paths
  -v                       Display the programs invoked by the compiler
  -E                       Preprocess only; do not compile, assemble or link
  -S                       Compile only; do not assemble or link
  -c                       Compile and assemble, but do not link
  -o <file>                Place the output into <file>
  -pie                     Create a position independent executable
  -x <language>            Specify the language of the following input files
                           Permissible languages include: c c++ assembler none
                           'none' means revert to the default behavior of
                           guessing the language based on the file's extension
```


### Refrerence
1. https://electronics.stackexchange.com/questions/66145/avr-how-to-program-an-avr-chip-in-linux
2. 



## Download Toolchain
Download the latest tool-chain from Microchip
https://www.microchip.com/mplab/avr-support/avr-and-arm-toolchains-c-compilers

+ -mmcu=atmega48
+ Include in script to point at toolchain export PATH=$PATH:$HOME/avr-tools/bin
+ add to avr/io.h
```c
    #elif defined (__AVR_ATmega4809__)
    #include <avr/iom4809.h>
```
+ Needed to download the Deveice support package from Microchip from http://packs.download.atmel.com/. Look into how to determine when and why this package was needed.
+ Copy the include/avr contents to the compiler
+ link the object files 
```
~/Documents/avr8-gnu-toolchain-linux_x86_64/bin/avr-ld -g -o main.bin main.o -m avrxmega3
```
+ the achritecture code for 103 is found here https://gcc.gnu.org/onlinedocs/gcc/AVR-Options.html

+ Its important to put the include path directories in the order you want ceedling to look first
```
  :environment:
  #- release_tool_path: 
  #- test_tool_path: /usr/bin/
  - path:
    - "/usr/bin:"
    - "/home/luke/Documents/avr8-gnu-toolchain-linux_x86_64/bin:"
    - "#{ENV['PATH']}"
```
will not work
```
  :environment:
  #- release_tool_path: 
  #- test_tool_path: /usr/bin/
  - path:
    - "/home/luke/Documents/avr8-gnu-toolchain-linux_x86_64/bin:"
    - "/usr/bin:"
    - "#{ENV['PATH']}"
```
will work


## Fuses
Eight diffrent fuese

FUSE
|Offset|Name|
|------|----|
|0x00|WDTCFG|
|0x01|BODCFG|
|0x02|OSCCFG|
|0x03|...|
|0x04|...|
|0x05|SYSCFG0|
|0x06|SYSCFG1|
|0x07|APPEND|
|0x08|BOOTEND|
|0x09|...|
|0x0A|LOCKBIT|

## WDTCFG : Watchdog configurations
*Note: Initial Factory Value 0x00*
### **Bit [7:4] : Window timeout period [3:0]** 
This value is loaded into the WINDOW bit field of WDT.CTRLA register duing reset
### **Bit [3:0] : Period [3:0]** 
This value is loaded into the PERIOD bit field of WDT.CTRLA register during reset

----
## BODCFG : Brownout Detection
### Bit[7:5] : LVL [2:0]
This value is loaded into the LVL bit field of the BOD.CTRLB register during reset
| Value | Name | Description | 
|---|---|---|
| 0x00 | BODLEVEL0 | 1.8V |
| 0x02 | BODLEVEL2 | 2.6V |
| 0x07 | BODLEVEL7 | 4.3V |
| Other | - | Reserverd |
*Note: refer to BOD and POR characterisitics for more details* 

### **Bit 4 : SAMPFREQ**
Loaded into SAMPFREQ bit field of the BOD.CTRLA register during reset
| Value | Description |
|---|---|
| 0x0 | Sample Frequency is 1kHz |
| 0x1 | Sample frequency 125Hz |

### **Bit [3:2] : Active [1:0]**
Loaded into the Active bit field of the BOD.CTRLA register during reset
| Value | Description |
|---|---|
| 0x0 | Disabled |
| 0x1 | Enabbled |
| 0x2 | Sampled |
| 0x3 | Enabled with wake-up halted untill BOD ready |

### **Bit [1:0] : SLEEP [1:0]**
Loaded into the SLEEP bit field of the BOD.CTRLA register during reset
| Value | Description |
|---|---|
| 0x0 | Disabled |
| 0x1 | Enabled |
| 0x2 | Sampled |
| 0x3 | Reserved|

---
## OSCCFG : Oscillator Configuration
### **Bit 7 : OSCLOCK**
Loaded into LOCK bit field of the CLKCTRL.OSC20MCALIBB during reset
| Value | Description |
|---|---|
| 0x0 | Calibrate registers of the 20MHz oscillator can be modifyed at run time |
| 0x1 | Calibrate registers of the 20MHz oscillator are locked at run time |

Bi
